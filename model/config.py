import os
from model.data_utils import load_data, export_trimmed_glove_vectors, get_trimmed_glove_vectors


class Config():
    def __init__(self):
        self.dir_output = "results/"
        self.dir_model = self.dir_output + "model.weights/"

        # directory for training outputs
        if not os.path.exists(self.dir_output):
            os.makedirs(self.dir_output)

        self.min_sentence_length = 2
        self.max_sentence_length = 20

        # embeddings
        self.embedding_size = 300

        # training
        self.is_train_embeddings = False
        self.dropout = 0.5
        self.lr_method = "adam"
        self.lr = 0.001
        self.clip = -1
        self.hidden_size_lstm = 300

        # load data
        self.filename_glove = "data/glove.6B/glove.6B.{}d.txt".format(self.embedding_size)
        self.filename_trimmed_glove = "data/glove.6B.{}d.trimmed.npz".format(self.embedding_size)
        self.document, self.vocab_word, self.vocab_tag = load_data(self.filename_glove)
        self.nwords = len(self.vocab_word)
        self.ntags = len(self.vocab_tag)
        self.used_pretrain = True
        export_trimmed_glove_vectors(self.vocab_word, self.filename_glove, self.filename_trimmed_glove,
                                     self.embedding_size)
        self.embeddings = (get_trimmed_glove_vectors(self.filename_trimmed_glove) if self.used_pretrain else None)
