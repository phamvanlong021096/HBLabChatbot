import json
import nltk
import os
import numpy as np
from nltk.stem.lancaster import LancasterStemmer

stemmer = LancasterStemmer()

UNK = "$UNK$"
NUM = "$NUM$"


def get_vocab_glove(filename):
    vocab = set()
    with open(filename) as f:
        for line in f:
            word = line.strip().split(' ')[0]
            vocab.add(word)
    return vocab


def load_data(file_name_glove):
    with open('intents.json') as json_data:
        intents = json.load(json_data)

    words = []
    tags = []
    documents = []
    ignore_words = ['?']

    for intent in intents['intents']:
        for pattern in intent['patterns']:
            w = nltk.word_tokenize(pattern)
            words.extend(w)
            documents.append((w, intent['tag']))
            if intent['tag'] not in tags:
                tags.append(intent['tag'])

    # stem and lower each word and remove duplicates
    # words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
    # words = sorted(list(set(words)))
    words = set(words)
    vocab_glove = get_vocab_glove(file_name_glove)
    words = words & vocab_glove
    words.add(UNK)
    words.add(NUM)
    words = list(words)

    # remove duplicates
    tags = sorted(list(set(tags)))

    # build vocab_word and vocab_tag
    vocab_word = dict()
    for i, w in enumerate(words):
        vocab_word[w] = i

    vocab_tags = dict()
    for i, t in enumerate(tags):
        vocab_tags[t] = i

    print(len(documents), "document")
    print(len(tags), "class")
    print(len(words), "unique stemmed words")
    return documents, vocab_word, vocab_tags


def export_trimmed_glove_vectors(vocab, glove_filename, trimmed_filename, dim):
    """Saves glove vectors in numpy array"""
    embeddings = np.zeros([len(vocab), dim])
    with open(glove_filename) as f:
        for line in f:
            line = line.strip().split(' ')
            word = line[0]
            embedding = [float(x) for x in line[1:]]
            if word in vocab:
                word_idx = vocab[word]
                embeddings[word_idx] = np.asarray(embedding)

    np.savez_compressed(trimmed_filename, embeddings=embeddings)


def get_trimmed_glove_vectors(filename):
    """
    filename: path to the npz file
    :return: matrix of embeddings (np array)
    """
    with np.load(filename) as data:
        return data["embeddings"]
