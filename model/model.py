import tensorflow as tf
from .base_model import BaseModel


class RNNClassifierModel(BaseModel):
    def __init__(self, config):
        super(RNNClassifierModel, self).__init__(config)
        self.sentence_ids = None
        self.sequence_id_lengths = None
        self.labels = None
        self.lr = None
        self.dropout = None
        self.sentence_embeddings = None
        self.logits = None
        self.class_predict = None
        self.loss = None

    def add_placeholders(self):
        # shape = (batch_size, max length of sentence in batch)
        self.sentence_ids = tf.placeholder(tf.int32, shape=[None, None], name='sentence_ids')

        # shape = (batch_size)
        self.sequence_id_lengths = tf.placeholder(tf.int32, shape=[None], name='sequence_id_lengths')

        # shape = (batch_size)
        self.labels = tf.placeholder(tf.int32, shape=[None], name='labels')

        # hyper parameters
        self.lr = tf.placeholder(tf.float32, shape=[], name='lr')
        self.dropout = tf.placeholder(tf.float32, shape=[], name='dropout')

    def add_word_embedding_op(self):
        with tf.variable_scope("words"):
            if self.config.embeddings is None:
                print("Randomly initializing word vectors")
                _word_embeddings = tf.get_variable(
                    name="_word_embeddings",
                    dtype=tf.float32,
                    shape=[self.config.nwords, self.config.embedding_size])
            else:
                _word_embeddings = tf.Variable(
                    self.config.embeddings,
                    name="_word_embeddings",
                    dtype=tf.float32,
                    trainable=self.config.is_train_embeddings)
            self.sentence_embeddings = tf.nn.embedding_lookup(_word_embeddings, self.sentence_ids,
                                                              name="sentence_embeddings")

    def add_logits_op(self):
        with tf.variable_scope("lstm"):
            cell = tf.nn.rnn_cell.LSTMCell(self.config.hidden_size_lstm)
            outputs, final_state = tf.nn.dynamic_rnn(cell, self.sentence_embeddings,
                                                     sequence_length=self.sequence_id_lengths, dtype=tf.float32)
            outputs = tf.nn.dropout(outputs, self.dropout)
            """throws UserWarning re: the gradient"""
            idx = tf.range(self.config.batch_size) * tf.shape(outputs)[1] + (self.sequence_id_lengths - 1)
            last_lstm_output = tf.gather(tf.reshape(outputs, [-1, self.config.hidden_size_lstm]), idx)

        # Softmax layer
        with tf.variable_scope('softmax'):
            W = tf.get_variable('W', [self.config.hidden_size_lstm, self.config.ntags])
            b = tf.get_variable('b', [self.config.ntags], initializer=tf.constant_initializer(0.0))
        self.logits = tf.matmul(last_lstm_output, W) + b

    def add_predict_op(self):
        preds = tf.nn.softmax(self.logits)
        self.class_predict = tf.cast(tf.argmax(preds, 1), tf.int32)

    def add_loss_op(self):
        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits, labels=self.labels)
        # mask = tf.sequence_mask(self.sequence_id_lengths)
        # losses = tf.boolean_mask(losses, mask)
        self.loss = tf.reduce_mean(losses)

    def build(self):
        self.add_placeholders()
        self.add_word_embedding_op()
        self.add_predict_op()
        self.add_loss_op()
        # Generic functions that add training op and initialize session
        self.add_train_op(self.config.lr_method, self.lr, self.loss, self.config.clip)
        self.initialize_session()


