from model.config import Config
from model.model import RNNClassifierModel

def main():
    config = Config()

    # load data

    # build model
    model = RNNClassifierModel(config)
    model.build()